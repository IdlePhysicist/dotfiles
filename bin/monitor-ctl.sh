#!/bin/bash

case $1 in
  laptop)
    xrandr \
      --output eDP1 --primary  --mode 1920x1080 \
      --output DP2-2 --off
    ;;
  desk)
    xrandr \
      --output eDP1 --off \
      --output DP2-2 --primary --auto 
    ;;
  projector)
    xrandr \
      --output eDP1 --primary --mode 1920x1080 \
      --output DP2-2 --same-as eDP1
    ;;
  *)
    echo "unknown $1"
    exit 1
esac

