#!/bin/sh

pprint () {
  echo " $(echo $1 | jq --raw-output '.ip') - $(echo $resp | jq --raw-output '.country_code') "
}

sleep 8

resp=$(curl --silent ifconfig.io/all.json)
stat=$? # This gives us the exit status of the curl command

if [[ $stat -eq 0 ]]; then
  sleep 10
  resp=$(curl --silent ifconfig.io/all.json)
fi

if pgrep -x openconnect > /dev/null; then
  pprint $resp
  echo "#00ff00"
else
  pprint $resp
fi
