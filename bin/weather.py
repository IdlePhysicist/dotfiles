#!/bin/python
# -*- coding: utf-8 -*-

# Procedure
# Surf to https://openweathermap.org/city
# Fill in your CITY
# e.g. Antwerp Belgium
# Check url
# https://openweathermap.org/city/2803138
# you will the city code at the end
# create an account on this website
# create an api key (free)
# LANG included thanks to krive001 on discord


import requests
import time
import sys

time.sleep(4)

CITIES = {
  "blessington": {
    "name": "Blessington",
    "id": "2966101"
  },
  "clonakilty": {
    "name": "Clonakilty",
    "id": "2965140"
  },
  "lafayette": {
    "name": "Lafayette",
    "id": "4922462"
  }
}

CITY = CITIES[sys.argv[1]]
if len(sys.argv) > 2:
  API_KEY = sys.argv[2]
else:
  API_KEY = "756edce7e9d4c385ef9499a53492678c"
UNITS = "Metric"
UNIT_KEY = "C"
LANG = "en"

def getWeather():
  try:
    resp = requests.get("http://api.openweathermap.org/data/2.5/weather?id={}&lang={}&appid={}&units={}".format(CITY['id'], LANG,  API_KEY, UNITS))
    if resp.status_code != 200:
      return str(resp.status_code)
  except:
    return "err"

  CURRENT = resp.json()["weather"][0]["description"].capitalize()
  TEMP = int(float(resp.json()["main"]["temp"]))
  return f" {CITY['name']}: {CURRENT}, {TEMP} °{UNIT_KEY} "

try:
  print(getWeather())
except:
  time.sleep(30) # Sleep for 30s to avoid 429s
  print(getWeather())
