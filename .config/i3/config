#
# i3 Configuration
#

set $mod Mod4

workspace_layout stacked

# Essentials
#
# Start a terminal
bindsym $mod+Return exec gnome-terminal #i3-sensible-terminal
# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Tray etc.
#
# Start up a few things
exec --no-startup-id ~/.config/feh/launch.sh
exec --no-startup-id dex -ae i3

# Volume
#
# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 py3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

font pango:DejaVu Sans Mono 8

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# open web browser
bindsym $mod+p exec brave

# kill focused window
bindsym $mod+q kill


# start a program launcher
bindsym $mod+Shift+space exec dmenu_run
#bindsym $mod+space exec rofi -show drun
bindsym $mod+space exec --no-startup-id i3-dmenu-desktop


# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+b split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
#bindsym $mod+Shift+space floating toggle
bindsym $mod+Shift+f floating toggle

# change focus between tiling / floating windows
#bindsym $mod+space focus mode_toggle
bindsym $mod+d focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws0 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws0

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws0

# Scratchpad
#
bindsym $mod+bracketleft  move scratchpad
bindsym $mod+bracketright scratchpad show

# resize window (you can also use the mouse for that)
mode "resize" {
  # These bindings trigger as soon as you enter the resize mode

  # Pressing left will shrink the window’s width.
  # Pressing right will grow the window’s width.
  # Pressing up will shrink the window’s height.
  # Pressing down will grow the window’s height.
  bindsym h resize shrink width 10 px or 10 ppt
  bindsym j resize grow height 10 px or 10 ppt
  bindsym k resize shrink height 10 px or 10 ppt
  bindsym l resize grow width 10 px or 10 ppt

  # same bindings, but for the arrow keys
  bindsym Left resize shrink width 10 px or 10 ppt
  bindsym Down resize grow height 10 px or 10 ppt
  bindsym Up resize shrink height 10 px or 10 ppt
  bindsym Right resize grow width 10 px or 10 ppt

  # back to normal: Enter or Escape or $mod+r
  bindsym Return mode "default"
  bindsym Escape mode "default"
  bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"

set $locker i3lock -e --pointer=win -i /home/eoghan/.config/i3/lockscreen.png --nofork
# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
#exec --no-startup-id xss-lock --transfer-sleep-lock -- $locker 

#
# Log out Shutdown etc.
mode "menu: [e]xit, [l]ock, [r]eboot, [s]hutdown" {
  bindsym e exec i3-msg exit
  bindsym l exec $locker; mode "default"
  bindsym r exec systemctl reboot
  bindsym s exec shutdown -h now
  bindsym Escape mode "default"
  bindsym Return mode "default"
}
bindsym $mod+Shift+q mode "menu: [e]xit, [l]ock, [r]eboot, [s]hutdown"

# lock
bindsym $mod+Shift+x exec $locker 

#
# Monitors
mode "menu: [1] laptop, [2] desk, [3] projector" {
  bindsym 1 exec ~/bin/monitor-ctl.sh laptop; mode "default"
  bindsym 2 exec ~/bin/monitor-ctl.sh desk; mode "default"
  bindsym 3 exec ~/bin/monitor-ctl.sh projector; mode "default"
  bindsym Escape mode "default"
  bindsym Return mode "default"
}
bindsym $mod+Shift+m mode "menu: [1] laptop, [2] desk, [3] projector"

#
# Bar
bar {
	status_command py3status 
  position top

	tray_output primary
#	tray_output eDP1

	bindsym button4 nop
	bindsym button5 nop
  font pango:Cantrell Regular 9
	strip_workspace_numbers yes

  colors {
    background #222D31
    statusline #F9FAF9
    separator  #454947

                       #border backgr. text
    focused_workspace  #F9FAF9 #16a085 #292F34
    active_workspace   #595B5B #353836 #FDF6E3
    inactive_workspace #595B5B #222D31 #EEE8D5
    binding_mode       #16a085 #2C2C2C #F9FAF9
    urgent_workspace   #16a085 #FDF6E3 #E5201D
  }
}

# Float these windows by default
for_window [class="Galculator"] floating enable
for_window [class="Gnome-dictionary"] floating enable
#for_window [class="Typora"] floating enable resize set 1200 1000
for_window [class="Signal"]  floating enable resize set 1200 1000
for_window [class="Keybase"] floating enable resize set 1000 800

# Delete Title bars
#default_border pixel 3
for_window [class="^.*"] border pixel 3
new_window pixel 3

# Ja like gaps?
##gaps inner 10
##gaps outer 0

mode "Toggle gaps: (1) on (2) off" {
    bindsym 1 mode "default", gaps inner all set 10, gaps outer all set 0
    bindsym 2 mode "default", gaps inner all set 0, gaps outer all set 0
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+semicolon mode "Toggle gaps: (1) on (2) off"

