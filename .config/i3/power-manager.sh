#!/bin/env sh

# Terminate already running bar instances
killall -q xfce4-power-manager

# Wait until the processes have been shut down
while pgrep -u $UID -x xfce4-power-manager > /dev/null; do sleep 1; done

xfce4-power-manager

