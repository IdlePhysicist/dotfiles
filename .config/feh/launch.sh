#!/bin/bash

killall -q feh
while pgrep -u $UID -x feh > /dev/null; do sleep 1; done

DIR=$HOME/Pictures/Wallpapers

if [[ -d $DIR ]]; then
  while true
  do
    feh --no-fehbg --bg-scale --randomize $DIR/*
    sleep 300
  done
fi

