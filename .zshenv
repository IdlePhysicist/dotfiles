#
#   General
#

#
# Environment Variables
#

# Source Uncommitble environments
source $HOME/.k.d/environment.sh

export EDITOR=$(which vim)

export MINIKUBE_IN_STYLE=false
export GO111MODULE=on

# src Path
export src=$HOME/src
# This is here to deal with my muscle memory

#
# Paths
#

# Go
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# Node
export PATH=$PATH:/usr/local/share/npm/bin

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Rust cargo binaries
export PATH=$PATH:$HOME/.cargo/bin

#
#   Platform Specific Things
#
if [[ $(uname) == "Darwin" ]]; then

  # HomeBrew
  export PATH=/usr/local/sbin:$PATH
  export PATH=/usr/local/bin:$PATH

  # HomeBrew Ruby
  export PATH=$PATH:/usr/local/lib/ruby/gems/2.6.0/bin
  export PATH="/usr/local/opt/ruby/bin:$PATH"

else

  # Go
  export GOROOT=/usr/lib/go

  # Set the default browser # Is this really needed
  if [ -n "$DISPLAY" ]; then
    export BROWSER=$(which brave)
  else
    export BROWSER=$(which links)
  fi
fi
