"
" .vimrc, IdlePhysicist
"

" -----------------------------------------------
" Plugins
"
call plug#begin('~/.vim/plugged')

"Plug 'lervag/vimtex', { 'for': 'tex' }

""Plug 'shime/vim-livedown', { 'for': 'markdown' }

""Plug 'gabrielelana/vim-markdown', { 'for': 'markdown' }

"Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

""Plug 'fatih/vim-go', { 'do': 'GoUpdateBinaries', 'for': 'go' }

""Plug 'govim/govim', { 'for': 'go' }

" IDE-esque
Plug 'scrooloose/nerdtree'
"Plug 'majutsushi/tagbar'
Plug 'godlygeek/tabular'

call plug#end()


" Enable Syntax Highlighting
syntax on

" Fix stupid backspace 'feature'
set backspace=indent,eol,start

" Tab / Shift Operations
set expandtab
set smarttab
set tabstop=2
set shiftwidth=2

" Theme
colo desert

" Enable the mouse
set mouse=a

" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c


set ffs=unix
set history=50

" filetype plugin on
filetype indent on

" Show Line Numbers
set number

" Vertical Ruler Colour
highlight ColorColumn ctermbg=238

"
" Highlight the current line
"
" Enables cursor line position tracking:
set cursorline
" Removes the underline causes by enabling cursorline:
highlight clear CursorLine
" Sets the line numbering to red background:
highlight CursorLineNR ctermbg=red  

" Render Whitespace
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:·,

" -----------------------------------------------
" Mappings
"
map <C-n> :NERDTreeToggle<CR>
"map <C-m> :TagbarToggle<CR>


" -----------------------------------------------
" File Specific Options
"
filetype detect

" Makefiles
autocmd FileType make set noexpandtab

" TSV
autocmd BufEnter *.tsv set noexpandtab

" Go & C
autocmd FileType go set cc=80,99 noexpandtab ts=4 shiftwidth=4
autocmd FileType c  set cc=80,99 noexpandtab ts=4 shiftwidth=4

" Markdown
autocmd BufNewFile,BufRead *.md set filetype=markdown
autocmd FileType markdown set wrap linebreak "tw=80
" YAML Front Matter in Markdown
let g:vim_markdown_frontmatter = 1

" TeX
let g:livepreview_previewer = 'open -a Preview'
let g:livepreview_engine = 'xelatex'
let g:livepreview_cursorhold_recompile = 0

" Python
autocmd FileType python set cc=80

"aug python
"    " ftype/python.vim overwrites this
"    au FileType python setlocal ts=4 sts=4 sw=4 noexpandtab
"aug end

"autocmd Filetype python set ts=2 sw=2 expandtab cc=80

" Jenkinsfile
autocmd BufNewFile,BufRead Jenkinsfile setf groovy

