#compdef _stim stim


function _stim {
  local -a commands

  _arguments -C \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]' \
    "1: :->cmnds" \
    "*::arg:->args"

  case $state in
  cmnds)
    commands=(
      "aws:Interact with AWS"
      "completion:Print the client completion"
      "deploy:Deploy helper"
      "help:Help about any command"
      "kube:Used to config and interact with Kubernetes"
      "pagerduty:Send events to Pagerduty"
      "slack:Interact with Slack"
      "vault:Vault helper"
      "version:Print the client version"
    )
    _describe "command" commands
    ;;
  esac

  case "$words[1]" in
  aws)
    _stim_aws
    ;;
  completion)
    _stim_completion
    ;;
  deploy)
    _stim_deploy
    ;;
  help)
    _stim_help
    ;;
  kube)
    _stim_kube
    ;;
  pagerduty)
    _stim_pagerduty
    ;;
  slack)
    _stim_slack
    ;;
  vault)
    _stim_vault
    ;;
  version)
    _stim_version
    ;;
  esac
}


function _stim_aws {
  local -a commands

  _arguments -C \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]' \
    "1: :->cmnds" \
    "*::arg:->args"

  case $state in
  cmnds)
    commands=(
      "login:aws login"
    )
    _describe "command" commands
    ;;
  esac

  case "$words[1]" in
  login)
    _stim_aws_login
    ;;
  esac
}

function _stim_aws_login {
  _arguments \
    '(-a --account)'{-a,--account}'[AWS Account]:' \
    '(-d --default-profile)'{-d,--default-profile}'[If --use-profiles is set, also set as [default] profile]' \
    '(-o --output)'{-o,--output}'[Output URLs to console (don'\''t launch URL)]' \
    '(-r --role)'{-r,--role}'[AWS Vault role]:' \
    '(-s --source)'{-s,--source}'[output env source for current shell]' \
    '(-t --ttl)'{-t,--ttl}'[Time-to-live for AWS credentials]:' \
    '(-p --use-profiles)'{-p,--use-profiles}'[Use profiles for storing credentials]' \
    '(-w --web)'{-w,--web}'[Generate AWS web login (Default: launch URL)]' \
    '(-b --web-ttl)'{-b,--web-ttl}'[Time-to-live for AWS web console access (min 15m, max 36h)]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}

function _stim_completion {
  _arguments \
    '(-h --help)'{-h,--help}'[help for completion]' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}

function _stim_deploy {
  _arguments \
    '(-f --deploy-file)'{-f,--deploy-file}'[Deployment file]:' \
    '(-e --environment)'{-e,--environment}'[Environment to deploy to]:' \
    '(-i --instance)'{-i,--instance}'[Instance to deploy to]:' \
    '(-m --method)'{-m,--method}'[Method to use for deployment.  Valid values are '\''auto'\'' '\''docker'\'' or '\''shell'\''.  Auto will use docker if it is available or fall back to shell if not.]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}

function _stim_help {
  _arguments \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}


function _stim_kube {
  local -a commands

  _arguments -C \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]' \
    "1: :->cmnds" \
    "*::arg:->args"

  case $state in
  cmnds)
    commands=(
      "config:Create/modify a Kubernetes context"
    )
    _describe "command" commands
    ;;
  esac

  case "$words[1]" in
  config)
    _stim_kube_config
    ;;
  esac
}

function _stim_kube_config {
  _arguments \
    '(-c --cluster)'{-c,--cluster}'[Required. Name of cluster to config]:' \
    '(-t --context)'{-t,--context}'[Optional. Name of context to set. Default is cluster name]:' \
    '(-r --current-context)'{-r,--current-context}'[Optional. Set to current context]' \
    '(-n --namespace)'{-n,--namespace}'[Optional. Name of default namespace]:' \
    '(-s --service-account)'{-s,--service-account}'[Required. Name of service account to use]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}

function _stim_pagerduty {
  _arguments \
    '(-a --action)'{-a,--action}'[Required. The event action. Must be one of [trigger, acknowledge, resolve]]:' \
    '(-l --class)'{-l,--class}'[The class/type of the event]:' \
    '(-c --component)'{-c,--component}'[The part or component of the affected system that is broken.]:' \
    '--dedupkey[UniquedDe-duplication key for the alert. Should the same between all actions for a single incident]:' \
    '(-d --details)'{-d,--details}'[Free-form details from the event]:' \
    '(-g --group)'{-g,--group}'[A cluster or grouping of sources. For example, sources “prod-datapipe-02” and “prod-datapipe-03” might both be part of “prod-datapipe”]:' \
    '(-s --service)'{-s,--service}'[Required. Name of Pagerduty service to send notification to]:' \
    '(-r --severity)'{-r,--severity}'[Required. How impacted the affected system is. Displayed to users in lists and influences the priority of any created incidents. Must be one of [Info, Warning, Error, Critical]]:' \
    '(-o --source)'{-o,--source}'[Specific human-readable unique identifier, such as a hostname, for the system having the problem. Defaults to hostname.]:' \
    '(-m --summary)'{-m,--summary}'[Required. A high-level, text summary message of the event. Will be used to construct an alert'\''s description.]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}

function _stim_slack {
  _arguments \
    '(-c --channel)'{-c,--channel}'[Required. The channel name to send the message to]:' \
    '(-i --icon-url)'{-i,--icon-url}'[Url to use as the icon for the message]:' \
    '(-m --message)'{-m,--message}'[Required. The message to send]:' \
    '(-u --username)'{-u,--username}'[Username for the message to appear as]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}


function _stim_vault {
  local -a commands

  _arguments -C \
    '(-a --address)'{-a,--address}'[Vault URL]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]' \
    "1: :->cmnds" \
    "*::arg:->args"

  case $state in
  cmnds)
    commands=(
      "login:login to Vault"
    )
    _describe "command" commands
    ;;
  esac

  case "$words[1]" in
  login)
    _stim_vault_login
    ;;
  esac
}

function _stim_vault_login {
  _arguments \
    '(-i --token-duration)'{-i,--token-duration}'[Set token expiration for given duration. Example '\''8h'\'']:' \
    '(-a --address)'{-a,--address}'[Vault URL]:' \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}

function _stim_version {
  _arguments \
    '--auth-method[Default authentication method (ex: ldap, github, etc.)]:' \
    '--cache-path[Path for storing cache files (defaults to ${STIM_PATH}/cache)]:' \
    '--config[Path to an explicit config file (defaults to ${STIM_PATH}/config.yaml)]:' \
    '--is-automated[Error on anything that needs to prompt and was not passed in as an ENV var or command flag]' \
    '(-x --noprompt)'{-x,--noprompt}'[Do not prompt for input. Will default to true for Jenkin builds.]' \
    '--path[Path for stim configuration files (defaults to ${HOME}/.stim)]:' \
    '(-v --verbose)'{-v,--verbose}'[verbose output]'
}


compdef _stim stim
