#compdef _markup markup


function _markup {
  local -a commands

  _arguments -C \
    '(-t --toggle)'{-t,--toggle}'[Help message for toggle]' \
    "1: :->cmnds" \
    "*::arg:->args"

  case $state in
  cmnds)
    commands=(
      "apply:Apply a LaTeX template to a Markdown file and render to PDF"
      "completion:Output shell completion code"
      "help:Help about any command"
      "list:A list the known templates"
      "version:Print version information"
      "xerox:Degrade your PDF to look like an old Xerox photocopy"
    )
    _describe "command" commands
    ;;
  esac

  case "$words[1]" in
  apply)
    _markup_apply
    ;;
  completion)
    _markup_completion
    ;;
  help)
    _markup_help
    ;;
  list)
    _markup_list
    ;;
  version)
    _markup_version
    ;;
  xerox)
    _markup_xerox
    ;;
  esac
}

function _markup_apply {
  _arguments \
    '(-t --template)'{-t,--template}'[Template name]:' \
    '(-x --xerox)'{-x,--xerox}'[Xerox the output]'
}

function _markup_completion {
  _arguments \
    '(-h --help)'{-h,--help}'[help for completion]' \
    '1: :("bash" "zsh")'
}

function _markup_help {
  _arguments
}

function _markup_list {
  _arguments
}

function _markup_version {
  _arguments
}

function _markup_xerox {
  _arguments
}

