#
#   Eoghan's .zshrc file
#

#
#   ZSH CONFIG + EXTENSIONS
#

# 
if [[ -z $ZSH_COMPDUMP ]]; then
  if [[ ! -e $HOME/.completions ]]; then
    mkdir $HOME/.completions
    chmod 700 $HOME/.completions
  fi
  ZSH_COMPDUMP=$HOME/.completions/compdump
fi

plugins=( git )

if [[ $(uname) == "Darwin" ]]; then
  plugins+=( osx )
  # Completion Path
  fpath=(/usr/local/share/zsh-completions $fpath)
else 
  plugins+=( kubectl helm stim )
fi

export ZSH="$HOME/.dotfiles/zsh"

source $ZSH/loader.sh

#
#   ZSH-NEWUSER-INSTALL & COMPINSTALL
#
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt beep extendedglob nomatch notify
bindkey -v
#    End of lines configured by zsh-newuser-install
#    The following lines were added by compinstall
#zstyle :compinstall filename '/Users/eoghan/.zshrc'

#autoload -Uz compinit
#compinit
#    End of lines added by compinstall
#

#   FIXING KEYBINDINGS
#
bindkey "^[[3~" delete-char # Fix Delete
bindkey "^[f" forward-word
bindkey "^[b" backward-word

# Prevent SSH completion
zstyle ':completion:*:ssh:*' hosts off


#
# Git branch as right prompt
#

# Load version control information
autoload -Uz vcs_info
precmd() { vcs_info }
# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats '%b'
# Set up the prompt (with git branch name)
setopt PROMPT_SUBST


#
# Prompts
#
PROMPT='%F{white}%B%n: %b%f%F{green}%1~%f %F{214}$%f ' # Left prompt
RPROMPT='%F{red}[${vcs_info_msg_0_}]%f' #'[%F{green}%?%f]' # Right prompt


export CLICOLOR=1
#
# MY ODDS AND ENDS
#

# Stop Oh-My-ZSH sharing history
unsetopt share_history
setopt no_share_history

# to create a directory and auto change to it
md () { mkdir -p "$@" && cd "$@" || echo "Error - no directory passed!";}
# This needed to be fixed because oh-my-zsh aliased md to mkdir -p

# cd && ls
cl () { cd "$@" && l ;}

# Controlling Dropbox daemon (on Debian)
#dropbox () { ~/.dropbox/dropbox.py "$@";}

# Forcing IPython to run (on Debian) ~ Why...?
#alias ipython='python -m IPython'

# Force tree to ignore .git dirs
alias tree="tree -I '.git/'"

# IPython Notebook alias
alias jn='jupyter notebook'

# Turn YAML to JSON
alias yaml2js="python -c 'import sys, yaml, json; json.dump(yaml.safe_load(sys.stdin), sys.stdout, indent=2)'"

alias html2csv="$HOME/src/python/html2csv-master/html2csv.py"

alias fix-firstbank-exports="$HOME/src/tooling/fix-firstbank-exports"

alias capture-keys="xev | grep -A2 --line-buffered '^KeyRelease' | sed -n '/keycode /s/^.*keycode \([0-9]*\).* (.*, \(.*\)).*$/\1 \2/p'"

alias sid="sid -sky $HOME/.config/sid/loc"

alias format-aib-exports='$HOME/src/tools/format-aib-exports'

alias jpip="/usr/local/Cellar/jupyterlab/1.2.4/libexec/bin/pip3"

alias bt="boinctui"

weather () { curl v2.wttr.in/$1 }

ppi3conf () { 
  grep '^bindsym' ~/.config/i3/config \
    | awk -F ' ' '{out=""; for(i=3;i<=NF;i++){out=out" "$i}; printf "%s\t%s\n", $2, out}' \
    | tab
}

if (( $+commands[gocloc] )); then
  alias cloc="gocloc"
fi

function godoc() {
  if [ ! -f go.mod ]
  then
    echo "error: go.mod not found" >&2
    return
  fi

  module=$(sed -n 's/^module \(.*\)/\1/p' go.mod)
  docker run \
    --rm \
    -e "GOPATH=/tmp/go" \
    -p 127.0.0.1:6060:6060 \
    -v $PWD:/tmp/go/src/$module \
    --name godoc \
    golang \
    bash -c "go get golang.org/x/tools/cmd/godoc \
    && echo http://localhost:6060/pkg/$module \
    && /tmp/go/bin/godoc -http=:6060"
}

alias vim="vim -p"

# GCC Aliases
#gccPath='/usr/local/Cellar/gcc'
#if [[ -e ${gccPath} ]]; then
#  latestVersion=$(ls ${gccPath} | tail -1)
#  compilers=('gcc' 'g++') 
#  for comp in "${compilers[@]}"; do
#    alias ${comp}='${gccPath}/${latestVersion}/bin/${comp}-${latestVersion:0:1}'
#  done
#fi

#
# SSH
#
#alias ssh-add='ssh-add $(find ~/.ssh | grep id_ | grep -Eiv .pub)'
#exec "$SHELL"
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > ~/.ssh-agent-thing
fi
if [[ "$SSH_AGENT_PID" == "" ]]; then
    eval "$(<~/.ssh-agent-thing)" > /dev/null
fi

