# Dotz

My dotfiles for zsh, vim, and a few other things.

## Configuration

I have tried to automate as much of this as I can, that said it is a work in progress and not really intended for others to use.

### Manual Edits

It is necessary to make a few manual edits despite the automation.

1. In the `zshrc` be sure to set the `$ZSH` path to point to the zsh directory in the dotfiles repo.

2. Also in the `zshrc` be sure to set the correct plugin for the OS that you are using. Choose from `osx`, `debian`, `archlinux`. These plugins come from the "oh-my-zsh" repo.

3. In the `zprofile`, check that all the paths make sense.

